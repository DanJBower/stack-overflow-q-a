import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
public class TextFieldsForEveryone extends JFrame {
	private JPanel panel;
	private JSpinner topRow;
	private JSpinner bottomRow;
	
	private static final int MINIMUM_NUMBER_SPINNER_VALUE = 1;
	private static final int MAXIMUM_NUMBER_SPINNER_VALUE = 10;
	private static final int STEP_NUMBER_SPINNER_VALUE = 1;
	
    private TextFieldsForEveryone() {
		setPreferredSize(new Dimension(800, 300));
		setUp();
		add(panel);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		setVisible(true);
    }
    
    private void setUp() {
    	panel = new JPanel(new MigLayout("fillx"));
    	panel.add(new JTextField("Area 1"), "growx");
    	panel.add(new JTextField("Area 2"), "growx");
		panel.add(new JTextField("Area 3"), "growx, spanx, wrap");
		panel.add(new JSeparator(), "growx, spanx, wrap");
		panel.add(new JTextField("Longer Area 1"), "growx");
		panel.add(new JTextField("Longer Area 2"), "growx, spanx, wrap");

		panel.add(new JLabel("Top Row JTextField Count"), "split 4, span");

		topRow = new JSpinner(new SpinnerNumberModel(3, MINIMUM_NUMBER_SPINNER_VALUE, MAXIMUM_NUMBER_SPINNER_VALUE, STEP_NUMBER_SPINNER_VALUE));
		topRow.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				
			}
		});
		panel.add(topRow, "growx");
		
		panel.add(new JLabel("Bottom Row JTextField Count"));
		
		bottomRow = new JSpinner(new SpinnerNumberModel(2, MINIMUM_NUMBER_SPINNER_VALUE, MAXIMUM_NUMBER_SPINNER_VALUE, STEP_NUMBER_SPINNER_VALUE));
		bottomRow.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				
			}
		});
		panel.add(bottomRow, "growx");
    }
    
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> new TextFieldsForEveryone());
    }
}