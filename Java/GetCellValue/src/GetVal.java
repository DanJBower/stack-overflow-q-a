import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class GetVal extends JFrame {
	private static final String windowName = "JTable Example";
	private static final long serialVersionUID = 362702020844358278L;
	
	private JPanel tablePanel;
	private JScrollPane scrollPanel;
	private JTable table;
	private DefaultTableModel model;

	private GetVal() {
		super(windowName);
		SetUp();
	}
	
	private void SetUp() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		SetUpJTable();
		SetUpJPanel();
		
		add(tablePanel);
		pack();
		setVisible(true);
	}
	
	private void SetUpJPanel() {
		tablePanel = new JPanel(new GridLayout());
		tablePanel.add(scrollPanel);
	}

	private void SetUpJTable() {
		String[] columns = new String[] {
		    "Something", "Something Else"
		};
		
		String[][] data = new String[][] {
			{"1", "2"},
			{"3", "3"}
		};
		
		model = new DefaultTableModel(data, columns) {
			private static final long serialVersionUID = -3895234084030399437L;

			@Override
		    public boolean isCellEditable(int row, int column)
		    {
		        return false;
		    }
		};
		
		table = new JTable(model);
		
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("Cell Val: " + table.getValueAt(table.getSelectedRow(), table.getSelectedColumn()));
			}
		});
		
		scrollPanel = new JScrollPane(table);
		scrollPanel.setPreferredSize(new Dimension(500, 500));
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(() -> new GetVal());
	}
}