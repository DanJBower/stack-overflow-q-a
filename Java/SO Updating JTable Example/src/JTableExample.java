import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class JTableExample extends JFrame {
	private static final String windowName = "JTable Example";
	private static final long serialVersionUID = 362702020844358278L;
	
	private JPanel tablePanel;
	private JScrollPane scrollPanel;
	private JTable table;
	private DefaultTableModel model;

	private JTableExample() {
		super(windowName);
		SetUp();
	}
	
	private void SetUp() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		SetUpJTable();
		SetUpJPanel();
		
		add(tablePanel);
		pack();
		setVisible(true);
	}
	
	private void SetUpJPanel() {
		tablePanel = new JPanel(new GridLayout());
		tablePanel.add(scrollPanel);
	}

	private void SetUpJTable() {
		String[] columns = new String[] {
		    "Something", "Something Else"
		};
		
		String[][] data = new String[][] {
			{"1", "2"},
			{"3", "3"}
		};
		
		model = new DefaultTableModel(data, columns) {
			private static final long serialVersionUID = -3895234084030399437L;

			@Override
		    public boolean isCellEditable(int row, int column)
		    {
		        return false;
		    }
		};
		
		table = new JTable(model);
		
		table.addMouseListener(new MouseAdapter() {
			@SuppressWarnings("unchecked")
			@Override
			public void mouseClicked(MouseEvent e) {
				//Get your row's values from your database
				//I'll use random values for this purpose
				int col1Val = getRand(0, 9);
				int col2Val = getRand(0, 9);
				
				//Put the values in an object vector
				Vector<Object> rowVals = new Vector<>();
				rowVals.addElement(col1Val);
				rowVals.addElement(col2Val);
				
				//Set the object vector
				model.getDataVector().setElementAt(rowVals, table.getSelectedRow());
				
				//Notify the table the row has changed
				model.fireTableRowsUpdated(table.getSelectedRow(), table.getSelectedRow());
			}
		});
		
		scrollPanel = new JScrollPane(table);
		scrollPanel.setPreferredSize(new Dimension(500, 500));
	}
	
	private static int getRand(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(() -> new JTableExample());
	}
}