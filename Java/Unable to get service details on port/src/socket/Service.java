package socket;

/**
 * An supplementary class to support SocketTester
 * 
 * @see <a href="https://stackoverflow.com/questions/51123167/unable-to-get-service-details-on-port">Unable to get service details on port?</a>
 * @version 1.0
 * @author Dan
 */

public class Service {
	private int port;
	private String pid;
	private String service;
	
	public Service(int port, String pid, String service) {
		this.port = port;
		this.pid = pid;
		this.service = service;
	}
	
	public int getPort() {
		return port;
	}
	
	public String getPID() {
		return pid;
	}
	
	public String getService() {
		return service;
	}
	
	@Override
	public String toString() {
		return "Service \"" + "\" is being run on port " + port + " and has the PID " + pid;
	}
}