package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

/**
 * An answer for <a href="https://stackoverflow.com/questions/51123167/unable-to-get-service-details-on-port">Unable to get service details on port?</a>
 * 
 * @see <a href="https://stackoverflow.com/questions/51123167/unable-to-get-service-details-on-port">Unable to get service details on port?</a>
 * @version 1.0
 * @author Dan
 */

public class SocketTester {
	/**
	 * This method checks whether a port is being used by any services.
	 * It will output any information to the system console.
	 * 
	 * @param port The port to be checked for any services
	 */
	public static void checkPort(int port) {
		TreeSet<String> pids = null;
		List<Service> services = null;
		
		pids = getPIDs(port);
		
		if(pids != null) {
			services = getServices(port, pids);
		}
		
		listInformation(port, services);
	}
	
	/**
	 * This method checks whether there are any PIDs on the specified port.
	 * If there are these are then returned.
	 * 
	 * @param port The port to check for PIDs
	 * @return It returns a TreeSet containing any found PIDs on the specified port
	 */
	private static TreeSet<String> getPIDs(int port) {
		TreeSet<String> returnVal = new TreeSet<String>();
		
		ProcessBuilder pidProcessBuilder = new ProcessBuilder("cmd.exe", "/C", "netstat -ano | find \"" + port + "\"");
		pidProcessBuilder.redirectErrorStream(true);
		
		Process pidProcess = null;
		try {
			pidProcess = pidProcessBuilder.start();
		} catch (IOException e) {
			return null;
		}
		
		BufferedReader pidProcessOutputReader = new BufferedReader(new InputStreamReader(pidProcess.getInputStream()));
		String outputLine = null;
		
		try {
			outputLine = pidProcessOutputReader.readLine();
		} catch (IOException e) {
			return null;
		}
		
		while (outputLine != null) {
			List<String> outputLineParts = new ArrayList<String>(Arrays.asList(outputLine.split(" ")));
			outputLineParts.removeAll(Arrays.asList(""));
			
			//outputLineParts.get(1) is the local address. We don't want a foreign address to accidently be found
			//outputLineParts.size() - 1 is the PID
			if(outputLineParts.get(1).contains(":" + port) && !returnVal.contains(outputLineParts.get(outputLineParts.size() - 1))) {
				returnVal.add(outputLineParts.get(outputLineParts.size() - 1));
			}
			
			try {
				outputLine = pidProcessOutputReader.readLine();
			} catch (IOException e) {
				return null;
			}
		}
		
		try {
			pidProcess.waitFor();
		} catch (InterruptedException e) {
			return null;
		}
		
		return returnVal;
	}
	
	/**
	 * This method checks whether there are any services related to the PID.
	 * If there are these are then returned.
	 * 
	 * @param port A reference to the PIDs port
	 * @param pids A list of PIDs found by getPIDs
	 * @return It returns a List containing any found services on the specified PIDs
	 */
	private static List<Service> getServices(int port, TreeSet<String> pids) {
		List<Service> returnVal = new ArrayList<Service>();
		
		for(String pid : pids) {
			ProcessBuilder serviceProcessBuilder = new ProcessBuilder("cmd.exe", "/C", "tasklist /svc /FI \"PID eq " + pid + "\" | find \"" + pid + "\"");
			serviceProcessBuilder.redirectErrorStream(true);
			
			Process serviceProcess = null;
			try {
				serviceProcess = serviceProcessBuilder.start();
			} catch (IOException e) {
				return null;
			}
			
			BufferedReader serviceProcessOutputReader = new BufferedReader(new InputStreamReader(serviceProcess.getInputStream()));
			String outputLine = null;
			
			try {
				outputLine = serviceProcessOutputReader.readLine();
			} catch (IOException e) {
				return null;
			}
			
			while(outputLine != null) {
				List<String> outputLineParts = new ArrayList<String>(Arrays.asList(outputLine.split(" ")));
				outputLineParts.removeAll(Arrays.asList(""));
				
				//outputLineParts.get(0) is the service
				returnVal.add(new Service(port, pid, outputLineParts.get(0)));
				
				try {
					outputLine = serviceProcessOutputReader.readLine();
				} catch (IOException e) {
					return null;
				}
			}
			
			try {
				serviceProcess.waitFor();
			} catch (InterruptedException e) {
				return null;
			}
		}
		return returnVal;
	}
	
	/**
	 * This method lists the information found by checkPort
	 * 
	 * @param port The port that has been checked for services
	 * @param servicesRunning The services found on the port
	 */
	private static void listInformation(int port, List<Service> servicesRunning) {
		if(servicesRunning != null && servicesRunning.size() != 0) {
			System.out.println("The following services are being run on port " + port);
			for(Service service : servicesRunning) {
				System.out.println("\t" + service.getService());
			}
		} else {
			System.out.println("There are no services being run on port " + port);
		}
	}
	
	public static void main(String[] args) {
		final int portToCheck = 135;
		checkPort(portToCheck);
	}
}