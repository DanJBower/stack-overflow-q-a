import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;

import net.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
public class MigCellWidthTrick extends JFrame {

	private MigCellWidthTrick() {
		setLayout(new MigLayout("insets 15, fill", "[][grow]", "[][grow]"));
	    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    setMinimumSize(new Dimension(500, 400));

	    add(new JLabel("Filter:"), "split, span");
	    add(new JTextField(), "wrap, growx");

	    add(new JScrollPane(new JTree()), "width 200, growy");
	    add(new JScrollPane(new JTable()), "span 2, grow");

	    pack();
	    setLocationRelativeTo(null);
	    setVisible(true);
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(() -> new MigCellWidthTrick());
	}
}
